﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformCam : MonoBehaviour
{

    GameObject Camera;
    public Transform camPostion;


    void Start()
    {
        Camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    public void ChangeCamPostion()
    {
        Camera.transform.position = camPostion.transform.position;
    }
}
