﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
	[CreateAssetMenu(fileName = "WaveControler", menuName = "CapCorps/WaveControler", order = 1)]
	public class WaveController: ScriptableObject
	{
		public enum EnemyName {
			Anxiety, Depression
		}

		public EnemyConfig[] enemyConfig;
		public WaveConfig[] waveConfig;

		public Enemy GetEnemy(EnemyWave enemy)
		{
			for(int i = 0; i < enemyConfig.Length; i++)
			{
				if(enemyConfig[i].enemyName == enemy.enemy)
				{
					return enemyConfig[i].enemy;
				}
			}
			return null;
		}

		public WaveConfig GetWaveConfig(int waveNum)
		{
			waveNum = Mathf.Clamp(waveNum, 0, waveConfig.Length - 1);
			return waveConfig[waveNum];
		}

		public EnemyWave GetEnemyWave(WaveConfig wave, int numEnemy)
		{
			numEnemy = Mathf.Clamp(numEnemy, 0, wave.enemyWave.Length - 1);
			return wave.enemyWave[numEnemy];
		}
	}

	[System.Serializable]
	public class EnemyConfig
	{
		public WaveController.EnemyName enemyName = WaveController.EnemyName.Anxiety;
		public Enemy enemy;
	}
}
