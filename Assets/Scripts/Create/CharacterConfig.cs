﻿using UnityEngine;
using UnityEngine.UI;

namespace Create
{
	public class CharacterConfig : MonoBehaviour
	{
		public enum Type
		{
			hero, enemy
		}
		public string characterName;
		public Type type = Type.hero;
		public Sprite image;

		public float life = 100;

		[Header("Atk")]
		public float damage = 5;
		public float timeAtk = 1, posY = 0.5f;

		[Header("Sound")]
		public AudioClip [] atkClip;
		public AudioClip [] hitClip;
		public AudioClip [] deathClip;
		public AudioClip [] demageClip;

		[Header("Prefabs")]
		public Slider lifeSlider;
		public float posLife = 3f;

        //Renderes
        public SkinnedMeshRenderer[] renderers;
        Material[] originalMaterials;
        public bool isOriginalMaterial { get; private set; }

        //Enemy
        [HideInInspector]
		public float speed;
        [HideInInspector]
        public int costDeath;

        //Hero
        [HideInInspector]
        public int dis = 3; //distance in Grid
        [HideInInspector]
        public float price = 1;

        public void Save(Character character)
		{
			PlayerPrefs.SetFloat(characterName + "life", character.life);
			PlayerPrefs.SetFloat(characterName + "damage", character.damage);

			Enemy enemy = character.GetComponent<Enemy>();
			if (enemy != null)
			{
				PlayerPrefs.SetFloat(characterName + "timeAtk", enemy.timeAtk);
			}
		}

        public void IsOriginalMaterial()
        {
            if (renderers == null || renderers.Length == 0)
            {
                GetRenderer();
                isOriginalMaterial = true;
            }
            else if(renderers[0].material == originalMaterials[0])
            {
                isOriginalMaterial = true;
            }
            else
            {
                isOriginalMaterial = false;
            }
        }

        public void ChangeMaterial(Material m)
        {
            if (!isOriginalMaterial)
            {
                return;
            }

            if (renderers == null || renderers.Length == 0)
            {
                GetRenderer();
            }

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = m;
            }
            isOriginalMaterial = false;
        }

        public void OriginalMaterial()
        {
            if (isOriginalMaterial)
            {
                return;
            }

            if (renderers == null || renderers.Length == 0)
            {
                GetRenderer();
                return;
            }

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = originalMaterials[i];
            }
            isOriginalMaterial = true;
        }

        void GetRenderer()
        {
            renderers = GetComponentsInChildren<SkinnedMeshRenderer>();

            if (renderers == null || renderers.Length == 0)
            {
                return;
            }

            originalMaterials = new Material[renderers.Length];
            for (int i = 0; i < renderers.Length; i++)
            {
                originalMaterials[i] = renderers[i].material;
            }
        }

        public CharacterConfig GetCharacter(CharacterConfig cConfig)
		{
			float _life = PlayerPrefs.GetFloat(characterName + "life");
			float _damage = PlayerPrefs.GetFloat(characterName + "damage");
			float _timeAtk = PlayerPrefs.GetFloat(characterName + "timeAtk");
			return new CharacterConfig(characterName, type, _life, _damage, _timeAtk);
		}

		public void SetANew(string roomName, int pos)
		{
			if(characterName == "")
			{
				Debug.LogError("You haven't defined the name");
			}
			PlayerPrefs.SetFloat(characterName + "life", life);
			PlayerPrefs.SetFloat(characterName + "damage", damage);
			PlayerPrefs.SetFloat(characterName + "timeAtk", timeAtk);

			PlayerPrefs.SetString(roomName + "Cat" + pos, characterName);
		}

		CharacterConfig(string _characterName,Type _type,
		float _life, float _damage, float _timeAtk)
		{
			characterName = _characterName;
			type = _type;
			life = _life;
			damage = _damage;
			timeAtk = _timeAtk;
		}
	}
}
