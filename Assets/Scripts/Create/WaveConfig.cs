﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Create
{
	[CreateAssetMenu(fileName = "WaveConfig", menuName = "CapCorps/WaveConfig", order = 2)]
	public class WaveConfig : ScriptableObject
	{
		public EnemyWave[] enemyWave;
	}

	[System.Serializable]
	public class EnemyWave
	{
		public WaveController.EnemyName enemy = WaveController.EnemyName.Anxiety;
		public Room.Rooms room = Room.Rooms.backyard;
		public float timeToSpawn;
	}
}
