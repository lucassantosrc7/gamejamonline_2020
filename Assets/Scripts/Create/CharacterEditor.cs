﻿using UnityEngine;
using UnityEditor;

namespace Create
{
#if UNITY_EDITOR
	[CustomEditor(typeof(CharacterConfig))]
	public class CharacterEditor : Editor
	{
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            CharacterConfig character = (CharacterConfig)target;

            if (character.type == CharacterConfig.Type.enemy)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Enemy Config", EditorStyles.boldLabel);

                character.speed = EditorGUILayout.FloatField("Speed", character.speed);
                character.costDeath = 100;
                character.dis = 1;

                Enemy e = character.GetComponent<Enemy>();

                if(e == null)
                {
                    character.gameObject.AddComponent<Enemy>();
                }
            }
            else if (character.type == CharacterConfig.Type.hero)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Hero Config", EditorStyles.boldLabel);

                character.dis = EditorGUILayout.IntField("Dis", character.dis);
                character.price = EditorGUILayout.FloatField("Price", character.price);
            }

            if (character.type != CharacterConfig.Type.enemy)
            {
                Enemy e = character.GetComponent<Enemy>();

                if (e != null)
                {
                    GameObject.Destroy(e);
                }
            }
        }
	}
#endif
}