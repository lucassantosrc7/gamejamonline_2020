﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomSlotHUD : MonoBehaviour
{
    GameController gameController;

    [HideInInspector]
    public List<RoomButton> roomButtons;

    public Button buttonPrefab;
    public Image alert;

    public float spaceY = 130;

    void Start()
    {
        gameController = GameController.instance;
        Room[] rooms = gameController.rooms;
        roomButtons = new List<RoomButton>();

        for (int i = 0; i < rooms.Length; i++)
        {
            RoomButton r_Button = new RoomButton(this, rooms[i]);
            r_Button.button.onClick.AddListener(() => GoToRoom(r_Button));
            roomButtons.Add(r_Button);
        }
    }

    void Update()
    {
        
    }

    public void GoToRoom(RoomButton r_Button)
    {
        gameController.ChangeRoom(r_Button.room.room);
    }
}

public class RoomButton
{
    public Room room;
    public Button button;

    public RoomButton(RoomSlotHUD slotHUD, Room room)
    {
        this.room = room;

        //Spawn Button
        button = GameObject.Instantiate(slotHUD.buttonPrefab, slotHUD.transform);
        RectTransform rect = button.GetComponent<RectTransform>();
        rect.anchoredPosition += Vector2.down * (slotHUD.spaceY * slotHUD.roomButtons.Count);

        //Image
        button.image.sprite = room.image;
        room.alert = GameObject.Instantiate(slotHUD.alert, button.transform).gameObject;
        room.alert.SetActive(false);
    }
}