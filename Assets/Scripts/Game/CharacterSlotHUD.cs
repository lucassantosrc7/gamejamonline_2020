﻿using System.Collections.Generic;
 using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using Create;

public class CharacterSlotHUD : MonoBehaviour
{
	GameController gameController;

	public Button buttonPrefab;
	[HideInInspector]
	public List<CharacterButton> characterButtons;

    TMP_Text priceTXT;

	public float spaceX = 130;

    void Start()
	{
		gameController = GameController.instance;
		CharacterConfig[] herosInit = gameController.herosInit;
		characterButtons = new List<CharacterButton>();

		for(int i = 0; i < herosInit.Length; i++)
		{
			CharacterButton c_Button = new CharacterButton(this, herosInit[i]);

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((eventData) => { SetCharacter(c_Button); });
            c_Button.trigger.triggers.Add(entry);

            characterButtons.Add(c_Button);
        }
    }

    public void SetCharacter(CharacterButton c_Button)
    {
        if (GameController.Gold < c_Button.character.price)
        {
            return;
        }

        GameController.currentHero = Instantiate(c_Button.character);
        GameController.currentHero.IsOriginalMaterial();
    }
}

public class CharacterButton
{
	public CharacterConfig character;
    public Button button;
    public EventTrigger trigger;
    public TMP_Text priceTXT;
    public Image image;
	public bool selected;

	public CharacterButton(CharacterSlotHUD slotHUD, CharacterConfig character)
	{
		this.character = character;

		//Spawn Button
		button = GameObject.Instantiate(slotHUD.buttonPrefab, slotHUD.transform);
        trigger = button.GetComponent<EventTrigger>();
        priceTXT = button.GetComponentInChildren<TMP_Text>();
		RectTransform rect = button.GetComponent<RectTransform>();
		rect.anchoredPosition += Vector2.right * (slotHUD.spaceX * slotHUD.characterButtons.Count);

		//Image
		image = button.transform.GetChild(0).GetComponent<Image>();
		image.sprite = character.image;
        priceTXT.text = character.price.ToString();

		selected = false;
	}
}
