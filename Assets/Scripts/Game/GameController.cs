﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using Create;

[RequireComponent(typeof(ControlScreen))]
public class GameController : MonoBehaviour
{
	public static GameController instance { get; private set; }
    public Camera camMain { get; private set; }
    ControlScreen controlScreen;

    [Header("Game properties")]
    [SerializeField]
    float camSpeed = 10;
    public Material wrongPlace;
    public Sprite normalGrid, dirtyGrid;
    [SerializeField]
	TMP_Text[] goldTxts;
	static TMP_Text[] gTxts;

    public static float Gold
	{
		get
		{
			return PlayerPrefs.GetFloat("Gold");
		}
		set
		{
			for (int i = 0; i < gTxts.Length; i++)
			{
				if (gTxts[i].gameObject != null)
				{
					gTxts[i].text = value.ToString();
				}
			}
			PlayerPrefs.SetFloat("Gold", value);
		}
	}

    public static int openGame
    {
        get
        {
            return PlayerPrefs.GetInt("OpenGame");
        }
        set
        {
            PlayerPrefs.SetInt("OpenGame", value);
        }
    }

	[Header("Heros")]
	public CharacterConfig[] allHeros;
	public CharacterConfig[] herosInit;

	[Header("Wave")]
	public WaveController waveController;
	public float startWaveTime;
    [SerializeField]
    Slider waveSlider;
    [SerializeField]
    Button StartWave;
    [SerializeField]
	TMP_Text waveTxt;
	public static int wave {
		get
		{
			return PlayerPrefs.GetInt("Wave");
		}
		private set
		{
			if (instance.waveTxt != null)
			{
				instance.waveTxt.text = "Começar a " + value.ToString() + "º onda";
			}

			PlayerPrefs.SetInt("Wave", value);
		}
	}

	[Header("Game")]
	public Character characterPrefab;
	public Canvas gameCanvas;
    public Transform lifesCanvas;
	public static CharacterConfig currentHero;
	AudioSource source;
	public AudioClip intro, loop;

	[Header("Room")]
	public Room[] rooms;
	public Room.Rooms firstRoom;
	public static Room currentRoom;

	void Awake()
	{
		PlayerPrefs.DeleteAll();
		instance = this;
		controlScreen = GetComponent<ControlScreen>();
        camMain = Camera.main;
        lifesCanvas = new GameObject("Lifes").transform;
        lifesCanvas.SetParent(gameCanvas.transform);
        lifesCanvas.SetSiblingIndex(0);

        #region Gold
		gTxts = goldTxts;
		for (int i = 0; i < gTxts.Length; i++)
		{
			if (gTxts[i].gameObject != null)
			{
				gTxts[i].text = Gold.ToString();
			}
		}

        if (openGame == 0)
        {
            Gold = 50;
        }
        #endregion

        #region Wave
        if (wave <= 0)
		{
			wave = 1;
		}
		else
		{
			waveTxt.text = "Começar a " + wave.ToString() + "º onda";
        }
        #endregion

        openGame++;
	}

	void Update()
	{
        if (Input.GetKeyDown(KeyCode.P))
        {
            rooms[1].grid.GetDirty(new Vector3(-4.5f, 0.3f, 8.3f));
        }

        if (ControlScreen.currentScreen != ControlScreen.Screens.Game)
        {
            return;
        }
        if(camMain.transform.position != currentRoom.camPos.position)
        {
            Transform a = camMain.transform; 
            Transform b = currentRoom.camPos;

            camMain.transform.position = Vector3.Lerp(a.position, b.position, camSpeed * Time.deltaTime);
            camMain.transform.rotation = Quaternion.Lerp(a.rotation, b.rotation, camSpeed * Time.deltaTime);
        }
	}

	public void StarGame()
	{
		GameObject game = controlScreen.GetAScreen(ControlScreen.Screens.Game);
		if (source == null)
		{
			source = game.GetComponent<AudioSource>();
		}

		StartCoroutine(MusicIntroLoop(source, intro, loop));

		for (int i = 0; i < rooms.Length; i++)
		{
			rooms[i].ActiveRoom(this);
			rooms[i].gameObject.SetActive(true);
		}
		ChangeRoom(firstRoom);
		waveSlider.value = 0;
        waveSlider.gameObject.SetActive(false);
        StartWave.gameObject.SetActive(true);
	}

	public void ChangeRoom(Room.Rooms room)
	{
		for (int i = 0; i < rooms.Length; i++)
		{
			if (room == rooms[i].room)
			{
				currentRoom = rooms[i];
                i = rooms.Length;
			}
		}
	}

	public Room GetRoom(Room.Rooms room)
	{
		for (int i = 0; i < rooms.Length; i++)
		{
			if (room == rooms[i].room)
			{
				return rooms[i];
			}
		}
		return null;
	}

    public void CheckRooms()
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            if (rooms[i].alert.activeSelf)
            {
                return;
            }
        }

        wave++;
        waveSlider.value = 0;
        waveSlider.gameObject.SetActive(false);
        StartWave.gameObject.SetActive(true);
    }

	public static Component CopyComponent(Component original, GameObject destination)
	{
		System.Type type = original.GetType();
		Component copy = destination.AddComponent(type);
		// Copied fields can be restricted with BindingFlags
		System.Reflection.FieldInfo[] fields = type.GetFields();
		foreach (System.Reflection.FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(original));
		}
		return copy;
	}

	IEnumerator MusicIntroLoop(AudioSource source, AudioClip sound1, AudioClip sound2)
	{
		source.clip = sound1;
		source.Play();
		yield return new WaitForSeconds(sound1.length);
		source.clip = sound2;
		source.loop = true;
		source.Play();
	}

	public void ActiveWave()
	{
		StartCoroutine(NextEnemy(0));
        waveSlider.gameObject.SetActive(true);
        StartWave.gameObject.SetActive(false);
    }

	IEnumerator NextEnemy(float time)
	{
		yield return new WaitForSeconds(time);
		WaveConfig waveConfig = waveController.GetWaveConfig(wave - 1); 
		EnemyWave enemyWave = waveController.GetEnemyWave(waveConfig, (int)waveSlider.value);
		Room room = GetRoom(enemyWave.room);
		Enemy enemy = waveController.GetEnemy(enemyWave);

		room.AddEnemy(enemy);
        room.alert.SetActive(true);
        waveSlider.maxValue = waveConfig.enemyWave.Length;
        int enemyNum = (int)waveSlider.value++;
		if(enemyNum < waveConfig.enemyWave.Length)
		{
			float t = waveConfig.enemyWave[enemyNum].timeToSpawn;
			StartCoroutine(NextEnemy(t));
		}
	}
}
