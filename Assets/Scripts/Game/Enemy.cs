﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Enemy : MonoBehaviour
{
	Character character;
	Create.CharacterConfig characterConfig;
    Rigidbody rb;

	[HideInInspector]
	public float timeAtk;

    [SerializeField]
    RectTransform gold;

	float time;

	float speed
	{
		get
		{
			return characterConfig.speed;
		}
	}

    public void Awake()
    {
		character = GetComponent<Character>();
		characterConfig = GetComponent<Create.CharacterConfig>();
        rb = GetComponent<Rigidbody>();

        rb.useGravity = false;
        rb.freezeRotation = true;

        if(character != null)
        {
            int layerMask = 1 << 9;
            character.layerMask = ~layerMask;
        }

        if (characterConfig != null)
        {
            timeAtk = characterConfig.timeAtk;
        }
	}

    void Update()
    {
        if(Time.time > time)
		{
			SendMessage("Attack", SendMessageOptions.DontRequireReceiver);
			time = Time.time + timeAtk;
		}

		if(character.target == null)
		{
			transform.Translate(Vector3.forward * speed * Time.deltaTime);
            character.CurrentDis = character.GetMaxDis();
		}

        if(transform.localPosition.z > character.room.disEnemyEnd)
        {
            GameController.instance.ChangeRoom(character.room.room);
            character.room.grid.GetDirty(transform.position);
            gameObject.SetActive(false);
            character.room.CheckEnemies();
        }
    }

	void Death()
	{
		GameController.Gold += characterConfig.costDeath;
        ShowGold();
        gameObject.SetActive(false);
        character.room.CheckEnemies();
    }

	void ShowGold()
	{
        RectTransform clone = Instantiate(gold, GameController.instance.lifesCanvas);
        clone.transform.position = Camera.main.WorldToScreenPoint(transform.position);
        TMPro.TMP_Text GoldTXT = clone.GetComponentInChildren<TMPro.TMP_Text>();
        GoldTXT.text = characterConfig.costDeath.ToString();
        Destroy(clone.gameObject, 1f);
    }
}
