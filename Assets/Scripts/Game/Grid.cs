﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Grid : MonoBehaviour
{
	public Room room {get; private set; }

    #region Fisrt/last point
    public Vector3 firstPoint
    {
        get
        {
            if (gridSlots == null || gridSlots.Count <= 0)
            {
                return Vector3.zero;
            }

            return gridSlots[0].position;
        }
    }

    public Vector3 lastPoint
    {
        get
        {
            if(gridSlots == null || gridSlots.Count <= 0)
            {
                return Vector3.zero;
            }

            int i = gridSlots.Count - 1;
            Vector3 l_Point = gridSlots[i].position;
            return gridSlots[i].position;
        }
    }
    #endregion

    public float size = 1f;
	public int numX = 10, numZ = 10; //z == Collum / x == Lines
    public static float timeDirty = 0.1f;

    public List<GridSlot> gridSlots;

    [SerializeField]
    bool drawGizmo = true;

	void Start()
	{
        drawGizmo = false;
    }

	public Vector3 GetNearestPointOnGrid(Vector3 position)
	{
		position = transform.InverseTransformPoint(position);

		int xCount = Mathf.RoundToInt(position.x / size);
		int yCount = Mathf.RoundToInt(position.y / size);
		int zCount = Mathf.RoundToInt(position.z / size);

		Vector3 result = new Vector3(
			(float)xCount * size,
			(float)yCount * size,
			(float)zCount * size);

		result = transform.TransformPoint(result);

        return result;
	}

	public GridSlot GetGridSlot(Vector3 point)
	{
		if(gridSlots == null || gridSlots.Count <= 0)
		{
			return null;
		}

        //Get point on grid and the first point
        point = GetNearestPointOnGrid(point);
        point = transform.InverseTransformPoint(point);

        Vector3 zero = gridSlots[0].position;
        zero = transform.InverseTransformPoint(zero);

        //Find the line and the column
        int iX = Mathf.RoundToInt(Mathf.Abs(zero.x - point.x) / size);
		int iZ = Mathf.RoundToInt(Mathf.Abs(zero.z - point.z) / size);

		//Get the index
		int i = iX * numZ + iZ;
		i = Mathf.Clamp(i, 0, gridSlots.Count - 1);

		return gridSlots[i];
	}

    public Vector3 ClampGrid(Vector3 pos)
    {
        //Tranform to local point
        pos = transform.InverseTransformPoint(pos);
        Vector3 f_Point = transform.InverseTransformPoint(firstPoint);
        Vector3 l_Point = transform.InverseTransformPoint(lastPoint);

        pos.x = Mathf.Clamp(pos.x, f_Point.x, l_Point.x);
        pos.z = Mathf.Clamp(pos.z, f_Point.z, l_Point.z);

        return pos = transform.TransformPoint(pos);
    }

	public void CreateSlots(Room room)
	{
		this.room = room;
		gridSlots = new List<GridSlot>();

        Vector3 pos = transform.position;

		for (int i = 0; i < numX; i++)
		{
			for (int j = 0; j < numZ; j++)
			{
				Vector3 point = GetNearestPointOnGrid(pos);
				gridSlots.Add(new GridSlot(point, ((i*numZ) + j), this));
                pos += transform.TransformDirection(Vector3.forward) * size;
            }
            pos += transform.TransformDirection(Vector3.right) * size;
            pos -= transform.TransformDirection(Vector3.forward) * size * numZ;
        }

        #region Collider
        BoxCollider bCollider = new GameObject("Collider").AddComponent<BoxCollider>();
        bCollider.isTrigger = true;
        bCollider.transform.SetParent(transform);
        bCollider.transform.localPosition = Vector3.zero;
        bCollider.transform.localEulerAngles = Vector3.zero;
        bCollider.tag = "Grid";

        //Size
        Vector3 v3 = Vector3.one * 0.1f;
        v3.x = numX * size;
        v3.z = numZ * size;
        bCollider.size = v3;

        //Pos
        v3 = Vector3.zero;
        v3.x = (bCollider.size.x / 2) - (size / 2);
        v3.z = (bCollider.size.x / 2) + (size / 2);
        bCollider.center = v3;
        #endregion
    }

    public void GetDirty(Vector3 pos)
    {
        pos = ClampGrid(pos);
        pos = GetNearestPointOnGrid(pos);
        StartCoroutine(DirtyColumn(pos));
    }

    IEnumerator DirtyColumn(Vector3 pos)
    {
        GetGridSlot(pos).Dirty();
        yield return new WaitForSeconds(timeDirty);
        if (pos.z > gridSlots[0].position.z)
        {
            pos.z -= size;
            StartCoroutine(DirtyColumn(pos));
        }
    }

    private void OnDrawGizmos()
	{
		if(size < 1)
		{
			return;
		}
        if (!drawGizmo)
        {
            return;
        }

        Vector3 pos = transform.position;
        bool firstWhite = false;

		for (int i = 0; i < numX; i++)
		{
			for (int j = 0; j < numZ; j++)
			{
				Vector3 point = GetNearestPointOnGrid(pos);
                Vector3 c_Size = new Vector3(size, 0.1f, size);

                #region Gizmo Color
                if (j == 0 && firstWhite)
                {
                    Gizmos.color = Color.black;
                    firstWhite = false;
                }
                else if (j == 0)
                {
                    Gizmos.color = Color.white;
                    firstWhite = true;
                }
                else if (Gizmos.color != Color.white)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.black;
                }
                #endregion

                Gizmos.DrawCube(point, c_Size);

				pos += transform.TransformDirection(Vector3.forward) * size;
			}
            pos += transform.TransformDirection(Vector3.right) * size;
            pos -= transform.TransformDirection(Vector3.forward) * size * numZ;
        }
	}
}

public class GridSlot
{
	Grid grid;

	public Character character { get; private set; }
	public Vector3 position { get; private set; }
    public int index { get; private set; }
    public string id
    {
        get
        {
            if(grid.room == null)
            {
                Debug.LogError("No room");
                return "";
            }
            string room = grid.room.room.ToString();
            return (room + "Slot" + index);
        }
    }

    public bool dirty
    {
        get
        {
            if (PlayerPrefs.GetInt(id + "Dirty") == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
           
        }
        private set
        {
            if (value)
            {
                PlayerPrefs.SetInt(id + "Dirty", 1);
            }
            else
            {
                PlayerPrefs.SetInt(id + "Dirty", 0);
            }
        }
    }

    SpriteRenderer g_Sprite;

	public GridSlot(Vector3 pos, int index, Grid grid)
	{
		position = pos;
		this.grid = grid;
		this.index = index;

        #region Sprite
        g_Sprite = new GameObject("Grid" + index.ToString()).AddComponent<SpriteRenderer>();
        g_Sprite.transform.SetParent(grid.transform);
        g_Sprite.transform.position = pos;
        g_Sprite.transform.localEulerAngles = Vector3.right * 90;
        g_Sprite.transform.localScale = (Vector2.one * grid.size);

        if (!dirty)
        {
            if (GameController.instance.normalGrid != null)
            {
                g_Sprite.gameObject.SetActive(true);
                g_Sprite.sprite = GameController.instance.normalGrid;
            }
            else
            {
                g_Sprite.gameObject.SetActive(false);
            }
        }
        else
        {
            Dirty();
        }
        #endregion

        string c_Name = PlayerPrefs.GetString(id);
		if(c_Name != "")
		{
			GameController gm = GameController.instance;
			for (int i = 0; i < gm.allHeros.Length; i++)
			{
				if (gm.allHeros[i].characterName == c_Name)
				{
                    Create.CharacterConfig cConfig = GameObject.Instantiate(gm.allHeros[i]);
                    ReceiveCharacter(cConfig);
				}
			}
		}
		else
		{
			character = null;
		}
    }

    public void ReceiveCharacter(Create.CharacterConfig characterConfig)
	{
        if (dirty)
        {
            return;
        }
		character = characterConfig.gameObject.AddComponent<Character>();
        character.transform.SetParent(grid.room.h_Parent);
        character.transform.position = position;
        character.transform.localEulerAngles = Vector3.up * 180;
        character.InitCharacter(grid.room);

        string room = grid.room.room.ToString();
		string c_Name = character.characterConfig.characterName;
        string slotName = room + "Slot" + index;
        PlayerPrefs.SetString(slotName, c_Name);

        character.gameObject.AddComponent<Player>().slotName = slotName;
    }

    public void Dirty()
    {
        if(character != null)
        {
            character.SendMessage("Death", SendMessageOptions.RequireReceiver);
        }

        if (GameController.instance.dirtyGrid != null)
        {
            g_Sprite.gameObject.SetActive(true);
            g_Sprite.sprite = GameController.instance.dirtyGrid;
        }
        else
        {
            g_Sprite.gameObject.SetActive(false);
        }
        dirty = true;
    }
}