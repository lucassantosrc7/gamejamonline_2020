﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Room : MonoBehaviour
{
	public enum Rooms
	{
		kitchen, backyard, room
	}
	public Rooms room;
    public Transform camPos;
    public Sprite image;
	public Grid grid;
    [HideInInspector]
    public GameObject alert;

	#region Heros
	[HideInInspector]
	public List<Character> heros;
	public Transform h_Parent { get; private set; }
	#endregion

	List<Enemy> enemies;
	Transform e_Parent;
    [Range(0,100)]
    [SerializeField]
    float disEnemySpaw = 5f;
    public float disEnemyEnd = 2f;

    Material material;
    int layerMask = 0;

    void Start()
    {
        layerMask = 1 << 8;
        layerMask = ~layerMask;

        material = GameController.instance.wrongPlace;
    }

    void Update()
	{
        if(GameController.currentRoom != this)
        {
            return;
        }
        if(GameController.currentHero != null)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (GameController.currentHero.isOriginalMaterial)
                {
                    Vector3 heroPos = GameController.currentHero.transform.position;
                    GridSlot gridSlot = grid.GetGridSlot(heroPos);
                    gridSlot.ReceiveCharacter(GameController.currentHero);
                    GameController.Gold -= GameController.currentHero.price;
                }
                else
                {
                    Destroy(GameController.currentHero.gameObject);
                }

                GameController.currentHero = null;
            }
            else if (Input.GetMouseButton(0))
            {
                RaycastHit hitInfo;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
                {
                    Transform c_Hero = GameController.currentHero.transform;
                    c_Hero.position = hitInfo.point;
                    c_Hero.eulerAngles = grid.transform.eulerAngles;
                    c_Hero.eulerAngles += Vector3.up * 180;

                    if (!hitInfo.collider.CompareTag("Grid")
                    || grid.GetGridSlot(hitInfo.point).character != null
                    || grid.GetGridSlot(hitInfo.point).dirty)
                    {
                        GameController.currentHero.ChangeMaterial(material);
                    }
                    else
                    {
                        GameController.currentHero.OriginalMaterial();
                    }
                }
            }
        }
    }

	public void ActiveRoom(GameController gm)
	{
        grid.CreateSlots(this);

        #region Hero Config
        heros = new List<Character>();
        h_Parent = new GameObject("Hero Parent").transform;
        h_Parent.SetParent(grid.transform);
        h_Parent.localEulerAngles = Vector3.zero;
        h_Parent.localPosition = Vector3.zero;
        #endregion

        #region Enemy Config
        enemies = new List<Enemy>();
        e_Parent = new GameObject("Enemies").transform;
        e_Parent.SetParent(grid.transform);
        e_Parent.localEulerAngles = Vector3.zero;
        e_Parent.localPosition = Vector3.forward * -disEnemySpaw;

        //Final point enemy
        float lastZ = grid.transform.InverseTransformPoint(grid.lastPoint).z;
        disEnemyEnd += lastZ + disEnemySpaw + (grid.size/2);
        #endregion
    }

    public void AddEnemy(Enemy enemy)
	{
		if(enemies != null && enemies.Count > 0)
		{
			for(int i = 0; i < enemies.Count; i++)
			{
				if(!enemies[i].gameObject.activeSelf
				&&  enemy == enemies[i])
				{
					enemies[i].gameObject.SetActive(true);
					return;
				}
			}
		}

		Enemy e = Instantiate(enemy, e_Parent);
		Character character = e.gameObject.AddComponent<Character>();
        character.InitCharacter(this);
		e.Awake();

        Vector3 pos = Vector3.zero;
        pos.x += Random.Range(0, grid.numX) * grid.size;
        print(pos);

		e.transform.localPosition = pos;
        e.transform.localEulerAngles = -e_Parent.transform.localEulerAngles;

        enemies.Add(e);
	}

    public void CheckEnemies()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].gameObject.activeSelf)
            {
                return;
            }
        }

        alert.SetActive(false);
        GameController.instance.CheckRooms();
    }

	IEnumerator ActiveEnemy(Character character)
	{
		yield return new WaitForSeconds(1);
		character.InitLife();
		character.gameObject.SetActive(true);
		character.SendMessage("Upgrade", SendMessageOptions.DontRequireReceiver);
	}
}
