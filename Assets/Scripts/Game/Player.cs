﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	Character character;

    [HideInInspector]
    public string slotName;

	void Awake()
    {
		character = GetComponent<Character>();
        int layerMask = 1 << 8;
        character.layerMask = ~layerMask;

        SendMessage("InitUlt", SendMessageOptions.DontRequireReceiver);
    }

	void Update()
    {

	}

    void Death()
    {
        PlayerPrefs.SetString(slotName, "");
    }
}
