﻿using System.Collections;
using Create;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterConfig))]
public class Character : MonoBehaviour
{
	public GameController gameController { get; private set; }
	public Room room { get; private set; }
	public Animator anim { get; private set; }
	public AudioSource source { get; private set; }
	[HideInInspector]
	public CharacterConfig characterConfig;

	[HideInInspector]
	public Character target;
    public Vector3 hitPoint { get; private set; }

	#region Life
	Slider lifeSlider;
	[HideInInspector]
	public float maxLife;
	public float life {
		get
		{
			return lifeSlider.value;
		}
		set
		{
			lifeSlider.value = value;

			if (lifeSlider.value <= 0)
			{
				SendMessage("Death", SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	[HideInInspector]
	public float shield;
	#endregion

	[HideInInspector]
	public float damage, timeAtk, CurrentDis;
	float time;
	Transform r_Origin; //raycast origin
    [HideInInspector]
    public int layerMask = 0;
    Vector3 dir;

	[HideInInspector]
	public float multiplyDamage;

	void Update()
	{
		#region Raycast
		Debug.DrawRay(r_Origin.position, transform.TransformDirection(Vector3.forward) * CurrentDis, Color.yellow);

		RaycastHit hit;
		if (Physics.Raycast(r_Origin.position, transform.TransformDirection(Vector3.forward), out hit, CurrentDis, layerMask))
		{
			if (target == null)
			{
				target = hit.collider.gameObject.GetComponent<Character>();
                time = Time.time + timeAtk;
			}
			else if (Time.time > time)
			{
				transform.SendMessage("Attack", SendMessageOptions.RequireReceiver);
				time = Time.time + timeAtk;
			}

            hitPoint = hit.point;
		}
		else if (target != null)
		{
			target = null;
		}
		#endregion

        if(GameController.currentRoom != room)
        {
            lifeSlider.gameObject.SetActive(false);
        }
        else if(lifeSlider.gameObject.activeInHierarchy)
        {
            lifeSlider.gameObject.SetActive(true);
        }

		if (lifeSlider != null && lifeSlider.gameObject.activeInHierarchy)
		{
			Vector3 pos = transform.position;
			pos += transform.TransformDirection(Vector3.up * characterConfig.posLife);
			lifeSlider.transform.position = Camera.main.WorldToScreenPoint(pos);
		}
	}

	public void InitCharacter(Room room)
	{
		gameController = GameController.instance;
		anim = GetComponent<Animator>();
		source = GetComponent<AudioSource>();
		characterConfig = GetComponent<CharacterConfig>();
		this.room = room;

		maxLife = characterConfig.life;
		damage = characterConfig.damage;
		timeAtk = characterConfig.timeAtk;

        CurrentDis = GetMaxDis();

		r_Origin = new GameObject("Raycast Origin").transform;
        r_Origin.SetParent(transform);
		r_Origin.transform.localPosition = Vector3.up * characterConfig.posY;

		InitLife();
	}

	public void InitLife()
	{
		if (lifeSlider == null)
		{
			lifeSlider = Instantiate(characterConfig.lifeSlider, gameController.lifesCanvas);
		}
		lifeSlider.minValue = 0;
		lifeSlider.maxValue = maxLife;
		life = maxLife;
	}

	public void Attack()
	{
		if (source != null && characterConfig.atkClip != null
		&& characterConfig.atkClip.Length > 0)
		{
			AudioClip[] clips = characterConfig.atkClip;
			source.PlayOneShot(clips[Random.Range(0,clips.Length)]);
		}
		anim.SetBool("Atk", true);
	}

	public void Ult()
	{
		anim.SetBool("Ult", true);
	}

	public void DoHit()
	{
		if (target == null || !target.gameObject.activeInHierarchy)
		{
			return;
		}

		if(source != null && characterConfig.hitClip != null
		&& characterConfig.hitClip.Length > 0)
		{
			AudioClip[] clips = characterConfig.hitClip;
			source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
		}
		multiplyDamage = Mathf.Clamp(multiplyDamage, 1, int.MaxValue);
		target.SendMessage("Hit", damage * multiplyDamage, SendMessageOptions.DontRequireReceiver);
	}

	public void Hit(float _damage)
	{
		float save = _damage * (shield / 100);
		life -= (_damage - save);

		if (source != null && characterConfig.demageClip != null
		&& characterConfig.demageClip.Length > 0)
		{
			AudioClip[] clips = characterConfig.demageClip;
			source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
		}
	}

	public void SetActive(bool value)
	{
		gameObject.SetActive(value);
	}

	void Death()
	{
		AudioSource g_Source = gameController.GetComponent<AudioSource>();
		if (g_Source != null && characterConfig.deathClip != null
		&& characterConfig.deathClip.Length > 0)
		{
			AudioClip[] clips = characterConfig.deathClip;
			g_Source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
		}
		SetActive(false);
	}

    public float GetMaxDis()
    {
        float size = room.grid.size;
        float dis = characterConfig.dis * size;
        Vector3 dir = transform.TransformDirection(Vector3.forward) * dis;
        dir += transform.position;
        dir = room.grid.ClampGrid(dir);

        //Current Dis
        float c_Dis = Vector3.Distance(transform.position, dir);
        c_Dis = Mathf.Clamp(c_Dis, 0, dis);
        return c_Dis;
    }

    public bool IsDifferentType(Character hit)
    {
        if(hit == null)
        {
            return false;
        }

        CharacterConfig.Type myType, hitType;
        myType = characterConfig.type;
        hitType = hit.characterConfig.type;

        if (hitType != myType)
        {
            return true;
        }

        return false;
    }

	void Upgrade()
	{
		//will we have upgrade?
	}

	void EnableAnim(string animName)
	{
		bool v = anim.GetBool(animName);
		anim.SetBool(animName, !v);
	}

	#region	Enable/Disable
	void OnDisable()
	{
		if(lifeSlider != null)
		{
			lifeSlider.gameObject.SetActive(false);
		}
	}

	void OnEnable()
	{
		if (lifeSlider != null)
		{
			lifeSlider.gameObject.SetActive(true);
		}
	}
	#endregion
}
