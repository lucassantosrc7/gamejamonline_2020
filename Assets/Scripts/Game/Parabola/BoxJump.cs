﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxJump : MonoBehaviour
{
    public ParabolaController pb;
    List<Transform> probableObstacles;

    BoxCollider col;
    public float dis;

    public Transform target;
    Transform[] p_Points;
    public Vector3 disObstacle;
    Transform parabolaRoot;

    bool go = false;

    void Awake()
    {
        probableObstacles = new List<Transform>();

        #region BoxCollider
        col = gameObject.AddComponent<BoxCollider>();
        col.center = Vector3.forward * (dis / 2);
        col.size = new Vector3(0.3f, 0.3f, dis);
        col.isTrigger = true;
        #endregion

        #region ParabolaRoot
        parabolaRoot = new GameObject("ParabolaRoot").transform;
        parabolaRoot.SetParent(transform);
        parabolaRoot.localPosition = Vector3.zero;
        pb.ParabolaRoot = parabolaRoot.gameObject;

        p_Points = new Transform[3];
        for (int i = 0; i < p_Points.Length; i++)
        {
            Transform p = new GameObject("ParabolaRoot" + i).transform;
            p.SetParent(parabolaRoot);
            p_Points[i] = p;
        }
        #endregion

        pb.gameObject.SetActive(false);
    }

    void Update()
    {
        if (pb.gameObject.activeInHierarchy && !go)
        {
            UpParabolaRoot();
            pb.FollowParabola();
            go = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            go = false;
            pb.gameObject.SetActive(true);
        }
    }

    public void UpParabolaRoot()
    {
        p_Points[0].position = transform.position;
        p_Points[2].position = target.position;

        Vector3 point = Vector3.Lerp(p_Points[0].position, p_Points[2].position, 0.5f);
        p_Points[1].position = point;

        if (probableObstacles.Count > 1)
        {
            p_Points[1].position += disObstacle;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!probableObstacles.Contains(other.transform))
        {
            probableObstacles.Add(other.transform);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (probableObstacles.Contains(other.transform))
        {
            probableObstacles.Remove(other.transform);
        }
    }
}
