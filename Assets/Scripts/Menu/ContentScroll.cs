﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ContentScroll : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	[SerializeField] RectTransform content;
	[Space]
	[SerializeField] RectTransform headerAreaRect;
	[SerializeField] float headerAreaHeight_Max = 700;
	[SerializeField] float headerAreaHeight_Min = 200;

	RectTransform viewport;
	private Rect contentOld;

	private List<Vector2> dragCoordinates = new List<Vector2>();
	private List<float> offsets = new List<float>();
	private int offsetsAveraged = 4;
	private float offset;
	private float velocity = 0;
	private bool changesMade = false;

	[HideInInspector] public float contentY;
	public float decelration = 0.135f;
	public float scrollSensitivity;

	float screenWidth;
	float screenHeight;
	float contentHeight;

	Coroutine inputRoutine;

	//[System.Serializable]
	//public UnityEvent OnValueChanged;
	//public OnValueChanged onValueChanged;

	private void Awake()
	{
		viewport = this.GetComponent<RectTransform>();

		objectsStartPos = new Vector2[objectsToMove.Length];
		for (int i = 0; i < objectsToMove.Length; i++)
		{
			objectsStartPos[i] = objectsToMove[i].transform.localPosition;
		}

		ResetScrollPosition();

		contentHeight = content.rect.height;
	}

	[SerializeField] GameObject[] objectsToMove;
	[SerializeField] float[] heightValueToStartMovement; //Keep experimenting with this value (in the inspector) for each gameObject until you get the desired effect.
	Vector2[] objectsStartPos;
	void MoveObjects()
	{
		for (int i = 0; i < objectsToMove.Length; i++)
		{
			objectsToMove[i].transform.localPosition = new Vector2(objectsToMove[i].transform.localPosition.x, objectsStartPos[i].y + (content.anchoredPosition.y - heightValueToStartMovement[i]));

			if (objectsToMove[i].transform.localPosition.y < objectsStartPos[i].y)
			{
				objectsToMove[i].transform.localPosition = new Vector2(objectsToMove[i].transform.localPosition.x, objectsStartPos[i].y);
			}
		}
	}

	//public float verticalNormalizedPosition
	//{
	//	get
	//	{
	//		float sizeDelta = CaculateDeltaSize();
	//		if (sizeDelta == 0)
	//		{
	//			return 0;
	//		}
	//		else
	//		{
	//			Debug.Log("VNP> " + (1 - content.transform.localPosition.y / sizeDelta));
	//			return 1 - content.transform.localPosition.y / sizeDelta;
	//		}
	//	}
	//	set
	//	{
	//		float o_verticalNormalizedPosition = verticalNormalizedPosition;
	//		float m_verticalNormalizedPosition = Mathf.Max(0, Mathf.Min(1, value));
	//		float maxY = CaculateDeltaSize();
	//		content.transform.localPosition = new Vector3(content.transform.localPosition.x, Mathf.Max(0, (1 - m_verticalNormalizedPosition) * maxY), content.transform.localPosition.z);
	//		float n_verticalNormalizedPosition = verticalNormalizedPosition;
	//		if (o_verticalNormalizedPosition != n_verticalNormalizedPosition)
	//		{
	//			MoveObjects(); // OnValueChanged?.Invoke();
	//		}
	//	}
	//}

	public void ResetScrollPosition()
	{
		headerAreaRect.sizeDelta = new Vector2(0, headerAreaHeight_Max);
		headerAreaRect.anchoredPosition = new Vector2(0, 0);

		viewport.sizeDelta = new Vector2(0, -headerAreaHeight_Max);
		viewport.anchoredPosition = new Vector2(0, viewport.sizeDelta.y);

		content.anchoredPosition = new Vector2(0, 0);
	}

	private float CaculateDeltaSize()
	{
		return Mathf.Max(0, content.rect.height - (viewport.rect.height / 2));
	}

	private void OnEnable()
	{
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		if (decelration < 0) decelration = Mathf.Abs(decelration);
		inputRoutine = StartCoroutine(HandleInputRoutine());
	}

	private void OnDisable()
	{
		if (inputRoutine != null)
		{
			StopCoroutine(inputRoutine);
			inputRoutine = null;
		}
		//MenuScrollLayoutBehaviour.instance.FireBackToMain();
	}

	IEnumerator HandleInputRoutine()
	{
		while (true)
		{
			yield return null;

			if (content.rect != contentOld)
			{
				changesMade = true;
				contentOld = new Rect(content.rect);
			}
			if (velocity != 0)
			{
				changesMade = true;
				velocity = (velocity / Mathf.Abs(velocity)) * Mathf.FloorToInt(Mathf.Abs(velocity) * (1 - decelration));
				offset = velocity;
			}
			if (changesMade)
			{
				OffsetContent(offset);
				changesMade = false;
				offset = 0;
			}
		}
	}

	private Vector2 ConvertEventDataDrag(PointerEventData eventData)
	{
		return new Vector2(eventData.position.x / screenWidth * screenWidth, eventData.position.y / screenHeight * screenHeight);
	}

	private Vector2 ConvertEventDataScroll(PointerEventData eventData)
	{
		return new Vector2(eventData.scrollDelta.x / screenWidth * screenWidth, eventData.scrollDelta.y / screenHeight * screenHeight) * scrollSensitivity;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		velocity = 0;
		dragCoordinates.Clear();
		offsets.Clear();
		dragCoordinates.Add(ConvertEventDataDrag(eventData));

		targetHeight = 0;
		targetHeightOld = 0;
	}

	public void OnDrag(PointerEventData eventData)
	{
		dragCoordinates.Add(ConvertEventDataDrag(eventData));

		//if (headerAreaRect.sizeDelta.y >= headerAreaHeight_Min)
		//{
		//	targetHeight = Mathf.Abs(content.sizeDelta.y / 6) - content.localPosition.y;
		//	CalculateHeaderPos(targetHeight);
		//}

		{
			UpdateOffsetsDrag();
		}

		if (offsets.Count - 1 > 0) OffsetContent(offsets[offsets.Count - 1]);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		dragCoordinates.Add(ConvertEventDataDrag(eventData));
		UpdateOffsetsDrag();
		OffsetContent(offsets[offsets.Count - 1]);
		float totalOffsets = 0;
		foreach (float offset in offsets)
		{
			totalOffsets += offset;
		}
		velocity = totalOffsets / offsetsAveraged;
		dragCoordinates.Clear();
		offsets.Clear();
	}

	private void OffsetContent(float givenOffset)
	{
		contentY = Mathf.Max(0, Mathf.Min(CaculateDeltaSize(), content.anchoredPosition.y + givenOffset));
		if (content.anchoredPosition.y != contentY)
		{
			content.anchoredPosition = new Vector3(content.anchoredPosition.x, contentY);
		}
		MoveObjects(); // OnValueChanged?.Invoke();
	}

	private void UpdateOffsetsDrag()
	{
		offsets.Add(dragCoordinates[dragCoordinates.Count - 1].y - dragCoordinates[dragCoordinates.Count - 2].y);
		if (offsets.Count > offsetsAveraged)
		{
			offsets.RemoveAt(0);
		}
	}

	private void UpdateOffsetsScroll(Vector2 givenScrollDelta)
	{
		offsets.Add(givenScrollDelta.y);
		if (offsets.Count > offsetsAveraged)
		{
			offsets.RemoveAt(0);
		}
	}

	float targetHeight;
	float targetHeightOld;
	void CalculateHeaderPos(float targetHeight)
	{
		if (targetHeight != targetHeightOld)
		{
			if (targetHeight < headerAreaHeight_Min)
			{
				targetHeight = headerAreaHeight_Min;
			}
			else if (targetHeight > headerAreaHeight_Max)
			{
				targetHeight = headerAreaHeight_Max;
			}
			headerAreaRect.sizeDelta = new Vector2(headerAreaRect.sizeDelta.x, targetHeight);

			viewport.sizeDelta = new Vector2(headerAreaRect.sizeDelta.x, -targetHeight);
			viewport.localPosition = new Vector2(0, screenHeight / 2 - headerAreaRect.sizeDelta.y);
		}
	}
}