﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Toggle))]
public class ToggleButtonBehaviour : MonoBehaviour, IPointerClickHandler
{
	public UnityEvent onTouch;
	[SerializeField] Image backgroundImage;
	[SerializeField] float activeScaleFactor;
	[SerializeField] GameObject activeIcon;
	[SerializeField] Color activeColor;
	[SerializeField] GameObject inactiveIcon;
	[SerializeField] Color inactiveColor;

	Toggle myToggle;

	private void Awake()
	{
		myToggle = this.GetComponent<Toggle>();
		myToggle.onValueChanged.AddListener(OnValueChange);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		onTouch?.Invoke();
	}

	void OnValueChange(bool value)
	{
		activeIcon.SetActive(value);        
		inactiveIcon.SetActive(!value);

		backgroundImage.color = (value) ? activeColor : inactiveColor;
		//this.transform.localScale = (value) ? new Vector3(1.2f, 1.2f, 1.2f) : new Vector3(1, 1, 1);

	}
}