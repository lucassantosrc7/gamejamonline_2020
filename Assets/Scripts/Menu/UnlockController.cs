﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnlockController : MonoBehaviour
{

    public GameObject[] canvasToHabilite;
    public GameObject[] Points;
    public TextMeshProUGUI UnlockText;
    public CanvasGroupFader UnlockCanvas;
    public int Gold = 10;   

    int NecessaryGold = 1;

    public int CanvasIndex;

    int UnlockedSpaces;
    
    public void Unlock(int Canvas)
    {
        UnlockCanvas.DoFadeIn();
        CanvasIndex = Canvas;

        if (UnlockedSpaces == 0)
        {
            NecessaryGold = 15000;
        }
        else if (UnlockedSpaces == 1)
        {
            NecessaryGold = 40000;
        }
        else
        {
            NecessaryGold = 200000;
        }

        UnlockText.text = "Deseja habilitar mais um espaço de equipe por $" + NecessaryGold +  "? ";

    }

    public void Enable()
    {

        if (Gold >= NecessaryGold)
        {
            canvasToHabilite[CanvasIndex].SetActive(false);
            Points[CanvasIndex].SetActive(true);
            Gold -= NecessaryGold;
            UnlockedSpaces++;
            UnlockCanvas.DoFadeOut();
        }
        else
        {
            Debug.Log("Sem GOLD");
        }        
    }

    

}
