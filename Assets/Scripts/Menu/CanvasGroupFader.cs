﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupFader : MonoBehaviour
{
    [SerializeField] bool startOn;
    [SerializeField] float fadeTime = 0.5f;

    CanvasGroup myCanvasGroup;

    bool isFading;
    bool isFadedIn;
    float currentFadeTime;
    float percOfFade;


    void Awake()
    {
        myCanvasGroup = this.GetComponent<CanvasGroup>();
        isFading = false;
        isFadedIn = false;
    }

    private void Start()
    {
        SetCanvasFaded(startOn);
    }

    public void ToggleFade()
    {
        if (isFading) return;

        if (isFadedIn) StartCoroutine(FadeOut());
        else StartCoroutine(FadeIn());
    }

    public void DoFadeIn(bool forceFade = false)
    {
        if (!forceFade && (isFading || isFadedIn)) return;

		if(this.gameObject.name == "TargetOverlay") Debug.Log("DoFadeIn");

        if(forceFade && isFading)
        {
            StopAllCoroutines();
            SetCanvasFaded(true);
        }
        else StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        isFading = true;
        currentFadeTime = 0f;
        percOfFade = 0f;

        while (isFading)
        {
            yield return null;

            currentFadeTime += Time.deltaTime;
            if (currentFadeTime > fadeTime)
            {
                currentFadeTime = fadeTime;
                isFading = false;
            }

            percOfFade = currentFadeTime / fadeTime;
            myCanvasGroup.alpha = Mathf.Lerp(0, 1, percOfFade);
        }

        isFadedIn = true;
        myCanvasGroup.interactable = true;
        myCanvasGroup.blocksRaycasts = true;
    }

    public void DoFadeOut(bool forceFade = false)
    {
        if (!forceFade && (isFading || !isFadedIn)) return;

		if (this.gameObject.name == "TargetOverlay") Debug.Log("DoFadeOut");

		if (forceFade && isFading)
        {
            StopAllCoroutines();
            SetCanvasFaded(false);
        }
        else StartCoroutine(FadeOut());
    }


    IEnumerator FadeOut()
    {
        isFading = true;
        currentFadeTime = 0f;
        percOfFade = 0f;

        myCanvasGroup.interactable = false;
        myCanvasGroup.blocksRaycasts = false;

        while (isFading)
        {
            yield return null;

            currentFadeTime += Time.deltaTime;
            if (currentFadeTime > fadeTime)
            {
                currentFadeTime = fadeTime;
                isFading = false;
            }

            isFadedIn = false;
            percOfFade = currentFadeTime / fadeTime;
            myCanvasGroup.alpha = Mathf.Lerp(1, 0, percOfFade);
        }
    }

    public void SetCanvasFaded(bool fadedIn)
    {
        myCanvasGroup.alpha = (fadedIn) ? 1 : 0;
        myCanvasGroup.interactable = fadedIn;
        myCanvasGroup.blocksRaycasts = fadedIn;
        isFadedIn = fadedIn;
        isFading = false;
    }

	public void SetInteractable(bool isInteractable)
	{
		myCanvasGroup.interactable = isInteractable;
		myCanvasGroup.blocksRaycasts = isInteractable;
	}

    public bool GetIsFading()
    {
        return isFading;
    }

	public void SetAlpha(float newAlpha)
	{
		myCanvasGroup.alpha = newAlpha;
	}
}