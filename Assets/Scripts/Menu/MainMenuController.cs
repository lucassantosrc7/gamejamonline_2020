﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum SwipeDirection { None, Up, Right, Down, Left }

public class MainMenuController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	[SerializeField] bool loopAreas;
	[SerializeField] [Range(0f, 1f)] float minPercOfScreenToSnap = 0.3f;
	[SerializeField] [Range(0f, 1f)] float touchTresholdPercent = 0.2f;

	RectTransform myRect;
	CanvasGroupFader myFader;
    [SerializeField] MenuAreaBehaviour[] areas;
	MenuAreaBehaviour currentArea;
	MenuAreaBehaviour targetArea;
	[SerializeField] Toggle[] toggles;

	Vector2 screenSize;
	Vector2 targetPos_Current;
	Vector2 targetPos_Target;
	Vector2 openPos;
	Vector2 closedPos_Left;
	Vector2 closedPos_Right;
	SwipeDirection currentDirection;
	float areaWidth;
	float areaHeight;
	float touchTresholdX_Left;
	float touchTresholdX_Right;
	float screenThresholdToSnapX;
	float screenThresholdToSnapY;
	float targetX;
	float targetAlpha_Current;
	float targetAlpha_Target;
	bool isDragging;

	private void Awake()
	{
		myRect = this.GetComponent<RectTransform>();
		myFader = this.GetComponent<CanvasGroupFader>();
		areas = GetComponentsInChildren<MenuAreaBehaviour>(false);
		if (areas.Length > 0) currentArea = areas[0];
		
		//toggles = GetComponentsInChildren<Toggle>(false);
	}

	private void Start()
	{
		RectTransform canvasRect = this.GetComponentInParent<Canvas>().GetComponent<RectTransform>();
		screenSize = new Vector2(canvasRect.rect.width, canvasRect.rect.height);

		areaWidth = myRect.rect.width;
		areaHeight = myRect.rect.height;

		openPos = new Vector2(0, 0);
		closedPos_Left = new Vector2(-areaWidth, 0);
		closedPos_Right = new Vector2(areaWidth, 0);

		touchTresholdX_Left = screenSize.x * touchTresholdPercent;
		touchTresholdX_Right = screenSize.x - (screenSize.x * touchTresholdPercent);

		screenThresholdToSnapX = areaWidth * minPercOfScreenToSnap;
		screenThresholdToSnapY = areaHeight * minPercOfScreenToSnap;

		if (areas.Length > 0)
		{
			areas[0].Initialize(0, openPos, screenSize, (loopAreas && areas.Length > 1) ? areas[areas.Length - 1] : null, (areas.Length > 1) ? areas[1] : null);
			for (int i = 1; i < areas.Length - 1; ++i)
			{
				areas[i].Initialize(i, closedPos_Right, screenSize, areas[i - 1], areas[i + 1]);
			}
			if (areas.Length > 1) areas[areas.Length - 1].Initialize(areas.Length - 1, closedPos_Right, screenSize, (areas.Length > 1) ? areas[areas.Length - 2] : null, (loopAreas && areas.Length > 1) ? areas[0] : null);
		}
		toggles[0].isOn = true;
		toggles[0].onValueChanged.Invoke(true);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		return; //TEMPORARY
		currentDirection = SwipeDirection.None;
		targetArea = null;
		targetX = 0;

		if (eventData.position.x > touchTresholdX_Right)
		{
			currentDirection = SwipeDirection.Left;
			targetArea = currentArea.GetNext();
			targetArea.SetObjectPosition(closedPos_Right);
			targetX = eventData.position.x - areaWidth / 2;
		}
		else if (eventData.position.x < touchTresholdX_Left)
		{
			currentDirection = SwipeDirection.Right;
			targetArea = currentArea.GetPrev();
			targetArea.SetObjectPosition(closedPos_Left);
			targetX = eventData.position.x - areaWidth / 2;
		}
		else
		{
			currentDirection = SwipeDirection.Up;
			currentArea.SetScroll(true);
		}

		if (currentDirection == SwipeDirection.None) return;

		isDragging = true;
		targetPos_Current = new Vector2(targetX, targetPos_Current.y);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		return; //TEMPORARY
		isDragging = false;

		if (currentDirection == SwipeDirection.Left)
		{
			if (eventData.position.x < screenSize.x / 2)
			{ // CHANGE
				targetArea.StopUpdate(openPos, true);//, true, currentDirection);
				currentArea.StopUpdate(closedPos_Left, false);//, false, currentDirection);
				currentArea = targetArea;
				toggles[currentArea.GetIndex()].isOn = true;
			}
			else
			{ // NO CHANGE
				targetArea.StopUpdate(closedPos_Right, false);//, false, currentDirection);
				currentArea.StopUpdate(openPos, true);//, true, currentDirection);
			}
		}
		else if (currentDirection == SwipeDirection.Right)
		{
			if (eventData.position.x > screenSize.x / 2)
			{ // CHANGE
				targetArea.StopUpdate(openPos, true);//, true, currentDirection);
				currentArea.StopUpdate(closedPos_Right, false);//, false, currentDirection);
				currentArea = targetArea;
				toggles[currentArea.GetIndex()].isOn = true;
			}
			else
			{ // NO CHANGE
				targetArea.StopUpdate(closedPos_Left, false);//, false, currentDirection);
				currentArea.StopUpdate(openPos, true);//, true, currentDirection);
			}
		}
		else
		{
			currentArea.SetScroll(false);
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		return; //TEMPORARY
				//if (!isActive || isLerping || !isDragging) return;
		if (!isDragging) return;

		if (currentDirection == SwipeDirection.Left)
		{
			targetX = eventData.position.x - screenSize.x;
			targetPos_Current = new Vector2(targetX, 0);
			targetAlpha_Current = eventData.position.x / screenSize.x;

			targetX = eventData.position.x;
			targetPos_Target = new Vector2(targetX, 0);
			targetAlpha_Target = 1 - (eventData.position.x / screenSize.x);
		}
		else if (currentDirection == SwipeDirection.Right)
		{
			targetX = eventData.position.x;
			targetPos_Current = new Vector2(targetX, 0);
			targetAlpha_Current = 1 - (eventData.position.x / screenSize.x);

			targetX = eventData.position.x - screenSize.x;
			targetPos_Target = new Vector2(targetX, 0);
			targetAlpha_Target = eventData.position.x / screenSize.x;
		}

		currentArea.UpdatePos(targetPos_Current, targetAlpha_Current);
		if (targetArea) targetArea.UpdatePos(targetPos_Target, targetAlpha_Target);
	}

	SwipeDirection CalculateSwipeDirection(Vector2 touchDelta)
	{
		SwipeDirection value = SwipeDirection.None;
		if (Mathf.Abs(touchDelta.normalized.x) > Mathf.Abs(touchDelta.normalized.y))
		{
			value = (touchDelta.x < 0) ? SwipeDirection.Left : SwipeDirection.Right;
		}
		else if (Mathf.Abs(touchDelta.normalized.x) < Mathf.Abs(touchDelta.normalized.y))
		{
			value = (touchDelta.y < 0) ? SwipeDirection.Down : SwipeDirection.Up;
		}

		return value;
	}

	public void ChangeArea(int index)
	{
        
		if (index == currentArea.GetIndex()) return;		

		targetArea = areas[index];
		if (!targetArea) return;

		if (index > currentArea.GetIndex())
		{
			targetArea.SetObjectPosition(closedPos_Right);
			targetPos_Current = closedPos_Left;
		}
		else
		{
			targetArea.SetObjectPosition(closedPos_Left);
			targetPos_Current = closedPos_Right;
		}

		currentArea.UpdatePos(targetPos_Current);
		targetArea.UpdatePos(openPos);

		currentArea = targetArea;
		toggles[index].isOn = true;
        
	}
}