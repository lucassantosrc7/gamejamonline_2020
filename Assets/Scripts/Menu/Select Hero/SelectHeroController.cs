﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Create;

public class SelectHeroController : MonoBehaviour
{
    public CanvasGroupFader HeroSelection;
	GameController gameController;

    public SelectHero[] selectHeroes;
	SelectHero currentSelectHero;

	[SerializeField]
	SelectHeroSlot[] selectHeroSlots;

	void Awake()
	{
		gameController = GameController.instance;
	}

	void Start()
	{
		/*for (int i = 0; i < GameController.heros.Count; i++)
		{
			selectHeroSlots[i].characterConfig = GameController.heros[i].characterConfig;
		}*/

		for (int i = 0; i < selectHeroSlots.Length; i++)
		{
			selectHeroSlots[i].InitButton(this);
		}

		for (int i = 0; i < selectHeroes.Length; i++)
		{
			selectHeroes[i].selectHeroController = this;
		}
	}

	public void SelectSlot(SelectHero selectHero)
	{
		HeroSelection.DoFadeIn();
		currentSelectHero = selectHero;
	}

    public void InstantiateHero(SelectHeroSlot selectHeroSlot)
    {
		if(selectHeroSlot.characterConfig == null)
		{
			return;
		}

		//gameController.AddPlayer(currentSelectHero.characterConfig); Só ativar isso quando fizer ativar um gato por slot
		selectHeroSlot.InitSelectHero(currentSelectHero);
		HeroSelection.DoFadeOut();
    }
}

[System.Serializable]
public class SelectHeroSlot
{
	[HideInInspector]
	public CharacterConfig characterConfig;
	public SelectHero selectHero { get; private set; }
	public UnityEngine.UI.Button button;

	public void InitButton(SelectHeroController selectHeroController)
	{
		button.onClick.RemoveAllListeners();
		button.onClick.AddListener(()=> selectHeroController.InstantiateHero(this));
	}

	public void InitSelectHero(SelectHero sHero)
	{
		selectHero = sHero;
		selectHero.CreateHero(characterConfig);
	}

	public void ResetButton()
	{
		selectHero.DestroyHero();
	}
}
