﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectHero : MonoBehaviour
{
	[HideInInspector]
	public SelectHeroController selectHeroController;

	public Create.CharacterConfig characterConfig { get; private set; }

	[HideInInspector]
	public GameObject hero { get; private set; }

    private void OnMouseDown()
    {
		selectHeroController.SelectSlot(this);
    }

	public void CreateHero(Create.CharacterConfig cConfig)
	{
		GameObject prefab = cConfig.gameObject;
		hero = Instantiate(prefab, transform);
		hero.transform.localPosition = Vector3.zero;
		hero.transform.localEulerAngles = Vector3.zero;
		hero.transform.localScale = new Vector3(0.125019f, 0.125019f, 0.125019f);
	}

	public void DestroyHero()
	{
		Destroy(hero);
		hero = null;
		characterConfig = null;
	}
}
