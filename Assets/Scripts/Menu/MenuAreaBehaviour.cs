﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroupFader))]
public class MenuAreaBehaviour : MonoBehaviour
{
	ContentScroll contentScroll;

	MenuAreaBehaviour nextArea;
	MenuAreaBehaviour prevArea;

	RectTransform myRect;
	CanvasGroupFader myFader;

	Vector2 screenSize;
	Vector2 targetPos;
	float targetAlpha;
	int myIndex;
	bool isUpdatingPos;

	private void Awake()
	{
		myRect = this.GetComponent<RectTransform>();
		myFader = this.GetComponent<CanvasGroupFader>();
		contentScroll = this.GetComponentInChildren<ContentScroll>();
	}

	public void Initialize(int index, Vector2 pos, Vector2 screenSize, MenuAreaBehaviour prev = null, MenuAreaBehaviour next = null)
	{
		myIndex = index;

		myRect.localPosition = pos;
		myRect.sizeDelta = screenSize;
		this.screenSize = screenSize;

		isUpdatingPos = false;

		SetPrevNext(prev, next);
		if(contentScroll) contentScroll.ResetScrollPosition();
		//SetScroll(false);
	}

	public int GetIndex()
	{
		return myIndex;
	}

	void SetPrevNext(MenuAreaBehaviour prev, MenuAreaBehaviour next)
	{
		prevArea = prev;
		nextArea = next;
	}

	public void SetScroll(bool isEnabled)
	{
		if (isUpdatingPos) return;

		if (contentScroll) contentScroll.enabled = isEnabled;
	}

	public void SetObjectPosition(Vector2 pos)
	{
		myRect.localPosition = pos;
	}

	public void UpdatePos(Vector2 targetPos, float targetAlpha = 1)
	{
		this.targetPos = targetPos;
		this.targetAlpha = targetAlpha;

		if(!isUpdatingPos)
		{
			isUpdatingPos = true;
			StartCoroutine(UpdatePosRoutine());
		}
	}

	Vector2 initialPos;
	IEnumerator UpdatePosRoutine()
	{
		initialPos = myRect.anchoredPosition;

		while (isUpdatingPos)
		{
			yield return null;

			myRect.anchoredPosition = Vector2.Lerp(myRect.anchoredPosition, targetPos, Time.deltaTime * 16);
			//UpdateAlpha(targetAlpha);
		}

		//float total = Mathf.Abs(targetPos.x - initialPos.x);
		//if (float.IsInfinity(total)) total = screenSize.x;
		
		while (true)
		{
			yield return null;

			myRect.anchoredPosition = Vector2.Lerp(myRect.anchoredPosition, targetPos, Time.deltaTime * 16);
			
			//if (isEntering)
			//{
			//	targetAlpha = 1 - myRect.anchoredPosition.x / total;
			//	UpdateAlpha(targetAlpha);
			//}

			if (Vector3.Distance(myRect.anchoredPosition, targetPos) < 5f)
			{
				myRect.anchoredPosition = targetPos;
				break;
			}
		}
		UpdateAlpha(1);
	}

	//bool isEntering;
	SwipeDirection direction;
	public void StopUpdate(Vector2 targetPos, bool isEntering)//, SwipeDirection direction)
	{
		this.targetPos = new Vector2(targetPos.x, 0);
		//this.isEntering = isEntering;
		//this.direction = direction;
		isUpdatingPos = false;

		if (!isEntering && contentScroll) contentScroll.ResetScrollPosition();
	}

	float minAlpha = 0.5f;
	void UpdateAlpha(float alpha)
	{
		alpha = Mathf.Clamp(alpha, minAlpha, 1);
		myFader.SetAlpha(alpha);
	}

	public MenuAreaBehaviour GetNext()
	{
		return nextArea;
	}
	public MenuAreaBehaviour GetPrev()
	{
		return prevArea;
	}
}