﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScreen : MonoBehaviour
{
	public static ControlScreen instance { get; private set; }
	GameController gameController;

	public enum Screens
	{
		Menu, Game
	}

	[SerializeField]
	Screens firstScreen;
	public static Screens currentScreen;

	public ScreenConfig[] screens;

    void Awake()
    {
		instance = this;
		gameController = GetComponent<GameController>();

		for(int i = 0; i < screens.Length; i++)
		{
			screens[i].gameObject.SetActive(false);
		}

		ChangeScreen(firstScreen);
    }

    public void ChangeScreen(Screens newScreen)
	{
		bool findNewScreen = false, findCurrentScreen = false;

		for(int i = 0; i < screens.Length; i++)
		{
			if(screens[i].screen == currentScreen)
			{
				screens[i].gameObject.SetActive(false);
				findCurrentScreen = true;
			}

			if(screens[i].screen == newScreen)
			{
				screens[i].gameObject.SetActive(true);
				findNewScreen = true;
			}

			if(findCurrentScreen && findNewScreen)
			{
				i = screens.Length;
			}
		}

		currentScreen = newScreen;
	}

	public GameObject GetAScreen(Screens screen)
	{
		for (int i = 0; i < screens.Length; i++)
		{
			if (screens[i].screen == screen)
			{
				return screens[i].gameObject;
			}
		}

		return null;
	}

	public void GoToGame()
	{
		ChangeScreen(Screens.Game);
		gameController.StarGame();
	}

	public void GoToMenu()
	{
		ChangeScreen(Screens.Menu);
	}

	public void EnablePopUp(GameObject popUp)
	{
		popUp.SetActive(!popUp.activeSelf);
	}
}

[System.Serializable]
public class ScreenConfig
{
	[SerializeField]
	string name;

	public ControlScreen.Screens screen;
	public GameObject gameObject;
}
