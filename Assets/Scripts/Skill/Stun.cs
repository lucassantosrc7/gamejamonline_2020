﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun : MonoBehaviour
{
	Create.CharacterConfig character;

	[SerializeField]
	float timeStunDefault = 2;
	float time;
	float timeStun {
		get
		{
			return PlayerPrefs.GetFloat(character.characterName + "StunTime");
		}
		set
		{
			PlayerPrefs.SetFloat(character.characterName + "StunTime", value);
		}
	}

	[SerializeField]
	int increaseLevel = 15;

	[SerializeField]
	float increase = 1;
	int level;

	Enemy enemy;
	Animator anim;

	bool stun = false;

	void Start()
    {
		character = GetComponent<Create.CharacterConfig>();
		level = 1;

		if (timeStun <= 0)
		{
			timeStun = timeStunDefault;
		}
    }

	void Update()
	{
		if(stun && Time.time > time)
		{
			enemy.enabled = true;
			anim.enabled = true;
			stun = false;

			enemy = null;
			anim = null;
		}	
	}

	public void UseUlt()
	{
		if (stun)
		{
			return;
		}

		level++;
		if(level >= increaseLevel)
		{
			level = 1;
			timeStun += increase;
		}

		time = Time.time + timeStun;
		//Character character = GameController.currentRoom.currentEnemy;
		enemy = character.GetComponent<Enemy>();
		anim = enemy.GetComponent<Animator>();

		enemy.enabled = false;
		anim.enabled = false;
		stun = true;
	}
}
