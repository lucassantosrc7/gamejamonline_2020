﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParabolaController))]
[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public ParabolaController pb { get; private set; }
    public Rigidbody rb { get; private set; }

    [HideInInspector]
	public Gun gun;

	[HideInInspector]
	public bool prepare, explode = false;

	[HideInInspector]
	public float speed, dis = 10;

    [HideInInspector]
    public string hitTag;

    Vector3 iniPos;

    public void Start()
    {
		rb = GetComponent<Rigidbody>();
        pb = GetComponent<ParabolaController>();
    }

    void Update()
    {
        if(Vector3.Distance(iniPos, transform.position) > dis)
		{
			gameObject.SetActive(false);
			explode = false;
		}

		if (prepare)
		{
			transform.position = gun.pointSpaw.position;
			transform.localEulerAngles = gun.bulletRot;
		}
		else
		{
			rb.MovePosition(transform.position + transform.TransformDirection(Vector3.left) * speed * Time.deltaTime);
		}
	}

	void OnEnable()
	{
		iniPos = transform.position;
		if(gun != null)
		{
			transform.position = gun.pointSpaw.position;
			transform.localEulerAngles = gun.bulletRot;
		}
	}

	void OnTriggerEnter(Collider other)
	{
        if (other.CompareTag(hitTag))
        {
            gameObject.SetActive(false);
            if (explode)
            {
                gun.character.multiplyDamage = 1.5f;
            }
            gun.SendMessage("DoHit", SendMessageOptions.RequireReceiver);
            gun.character.multiplyDamage = 1;
            explode = false;
        }
	}
}
