﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
	[HideInInspector]
	public Character character;
    BoxCollider col;

    [Header("Parabola")]
    Transform parabolaRoot;
    [SerializeField]
    Vector3 disObstacle;
    Transform[] p_Points;
    List<Transform> probableObstacles;

    public Transform pointSpaw;
	[SerializeField]
	Bullet bulletPrefab;
	Bullet currentBullet;
	List<Bullet> bullets;

	public Vector3 bulletRot;

	[SerializeField]
	float speed = 20;

    public void InitUlt()
    {
        character = GetComponent<Character>();
        probableObstacles = new List<Transform>();
        col = gameObject.AddComponent<BoxCollider>();

        #region BoxCollider
        Vector3 center = Vector3.up * character.characterConfig.posY;
        center.z = character.CurrentDis / 2;
        col.center = center;
        col.size = new Vector3(0.3f, 0.3f, character.CurrentDis);
        col.isTrigger = true;
        #endregion

        #region ParabolaRoot
        parabolaRoot = new GameObject("ParabolaRoot").transform;
        parabolaRoot.SetParent(character.transform);
        parabolaRoot.localPosition = Vector3.zero;

        p_Points = new Transform[3];
        for (int i = 0; i < p_Points.Length; i++)
        {
            Transform p = new GameObject("ParabolaRoot" + i).transform;
            p.SetParent(parabolaRoot);
            p_Points[i] = p;
        }
        #endregion

        #region Bullet
        bullets = new List<Bullet>();
        for(int i = 0; i < 4; i++)
		{
            //Initialize
            Bullet bullet = Instantiate(bulletPrefab, transform);
			bullet.gun = this;
            bullet.Start();

            //Parabola
            bullet.pb.ParabolaRoot = parabolaRoot.gameObject;
            bullet.pb.Speed = speed;
            bullet.pb.Autostart = false;

            //Numbers
            bullet.speed = 0;
            bullet.dis = character.CurrentDis;

            //List
			bullets.Add(bullet);
			bullet.gameObject.SetActive(false);
		}

		bulletPrefab.gameObject.SetActive(false);
        #endregion
    }

	public void Shoot()
	{
		if(currentBullet == null
		|| !currentBullet.gameObject.activeSelf)
		{
            return;
		}
		currentBullet.prepare = false;

        UpParabolaRoot();
        currentBullet.pb.FollowParabola();
    }

	public void UseUlt()
	{
        if (currentBullet == null
        || !currentBullet.gameObject.activeSelf)
        {
            return;
        }
		currentBullet.explode = true;
        Shoot();
    }

	public void EnableBullet()
	{
        if (character.target == null)
        {
            return;
        }
        currentBullet = GetBullet();
		currentBullet.prepare = true;
        currentBullet.hitTag = character.target.tag;
    }

    public void UpParabolaRoot()
    {
        if(character.target == null)
        {
            return;
        }
        p_Points[0].position = pointSpaw.position;
        p_Points[2].position = character.hitPoint;

        Vector3 point = Vector3.Lerp(p_Points[0].position, p_Points[2].position, 0.5f);
        p_Points[1].position = point;

        if (probableObstacles.Count > 1)
        {
            p_Points[1].position += disObstacle;
        }
    }

	public Bullet GetBullet()
	{
		for (int i = 0; i < bullets.Count; i++)
		{
			if (!bullets[i].gameObject.activeSelf)
			{
				bullets[i].gameObject.SetActive(true);
				return bullets[i];
			}
		}

        //Initialize
        Bullet bullet = Instantiate(bulletPrefab, transform);
        bullet.gun = this;
        bullet.Start();

        //Parabola
        bullet.pb.ParabolaRoot = parabolaRoot.gameObject;
        bullet.pb.Speed = speed;

        //Floats
        bullet.speed = 0;
        bullet.dis = character.CurrentDis;

        //List
        bullets.Add(bullet);
        return bullet;
	}

    void OnTriggerEnter(Collider other)
    {
        if (!probableObstacles.Contains(other.transform))
        {
            probableObstacles.Add(other.transform);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (probableObstacles.Contains(other.transform))
        {
            probableObstacles.Remove(other.transform);
        }
    }
}
