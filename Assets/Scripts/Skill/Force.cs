﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Force : MonoBehaviour
{
	Create.CharacterConfig character;

	[SerializeField]
	int multiplyForce = 2;

	[SerializeField]
	float timeForceDefault = 2;
	float time;
	float timeForce
	{
		get
		{
			return PlayerPrefs.GetFloat(character.characterName + "forceTime");
		}
		set
		{
			PlayerPrefs.SetFloat(character.characterName + "forceTime", value);
		}
	}

	[SerializeField]
	int increaseLevel = 10;

	[SerializeField]
	int increase = 1;
	int level;

	bool force = false;
	float addForce;

	void Start()
	{
		character = GetComponent<Create.CharacterConfig>();
		level = 1;

		if (timeForce <= 0)
		{
			timeForce = timeForceDefault;
		}
	}

	void Update()
	{
		/*if (force && Time.time > time)
		{
			Character[] heros = GameController.heros.ToArray();
			for (int i = 0; i < heros.Length; i++)
			{
				heros[i].damage -= addForce;
			}
			force = false;
		}*/
	}

	public void UseUlt()
	{
		if (force)
		{
			return;
		}

		level++;
		if (level >= increaseLevel)
		{
			level = 1;
			multiplyForce += increase;
		}

		time = Time.time + timeForce;
		/*Character[] heros = GameController.heros.ToArray();
		for (int i = 0; i < heros.Length; i++)
		{
			float d = heros[i].damage;
			heros[i].damage *= multiplyForce;
			addForce = heros[i].damage - d;
		}*/

		force = true;
	}
}
