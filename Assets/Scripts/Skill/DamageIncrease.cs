﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageIncrease : MonoBehaviour
{
	Create.CharacterConfig characterConfig;
	Character character;

	[SerializeField]
	int multiplyDamageDefault = 10;
	int multiplyDamage
	{
		get
		{
			return PlayerPrefs.GetInt(characterConfig.characterName + "multiplyDamage");
		}
		set
		{
			PlayerPrefs.SetInt(characterConfig.characterName + "multiplyDamage", value);
		}
	}
	float multiply;

	[SerializeField]
	int increaseLevel = 5;

	[SerializeField]
	int increase = 1;
	int level;

	bool isMultiplied;

	void Start()
	{
		characterConfig = GetComponent<Create.CharacterConfig>();
		character = GetComponent<Character>();
		level = 1;

		if (multiplyDamage <= 0)
		{
			multiplyDamage = multiplyDamageDefault;
		}
	}

	void Update()
	{

	}

	public void UseUlt()
	{
		level++;
		if (level >= increaseLevel)
		{
			level = 1;
			multiplyDamage += increase;
		}

		multiply = character.damage * multiplyDamage;
		character.damage += multiply;
		isMultiplied = true;
	}

	public void BackNormal()
	{
		if (!isMultiplied)
		{
			return;
		}

		character.damage -= multiply;
	}
}
