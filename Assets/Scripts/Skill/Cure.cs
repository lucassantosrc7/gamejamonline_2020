﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cure : MonoBehaviour
{
	Create.CharacterConfig character;

	[Range(0, 100)]
	[SerializeField]
	float percentCureDefault = 10;
	float percentCure
	{
		get
		{
			return PlayerPrefs.GetFloat(character.characterName + "CurePer");
		}
		set
		{
			PlayerPrefs.SetFloat(character.characterName + "CurePer", value);
		}
	}

	[SerializeField]
	int increaseLevel = 5;

	[SerializeField]
	float increase = 2;
	int level;

	void Start()
	{
		character = GetComponent<Create.CharacterConfig>();
		level = 1;

		if (percentCure <= 0)
		{
			percentCure = percentCureDefault;
		}
	}

	public void UseUlt()
	{
		level++;
		if (level >= increaseLevel)
		{
			level = 1;
			percentCure += increase;
		}

		/*Character[] heros = GameController.heros.ToArray();
		for (int i = 0; i < heros.Length; i++)
		{
			float p = heros[i].life * (percentCure / 100);
			heros[i].life += p;
		}*/
	}
}
