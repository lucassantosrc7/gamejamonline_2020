﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
	Create.CharacterConfig character;

	[Range(0,100)]
	[SerializeField]
	int percentShieldDefault = 50;
	int percentShield
	{
		get
		{
			return PlayerPrefs.GetInt(character.characterName + "ShieldPer");
		}
		set
		{
			PlayerPrefs.SetInt(character.characterName + "ShieldPer", value);
		}
	}

	[SerializeField]
	float timeShield;
	float time;

	[SerializeField]
	int increaseLevel = 10;

	[SerializeField]
	int increase = 2;
	int level;

	bool shield = false;

	void Start()
    {
		character = GetComponent<Create.CharacterConfig>();
		level = 1;

		if (percentShield <= 0)
		{
			percentShield = percentShieldDefault;
		}
	}

    void Update()
    {
		/*if (shield && Time.time > time)
		{
			Character[] heros = GameController.heros.ToArray();
			for (int i = 0; i < heros.Length; i++)
			{
				heros[i].shield -= percentShield;
			}
			shield = false;
		}*/
	}

	public void UseUlt()
	{
		if (shield)
		{
			return;
		}

		level++;
		if (level >= increaseLevel)
		{
			level = 1;
			percentShield += increase;
		}

		time = Time.time + timeShield;
		/*Character[] heros = GameController.heros.ToArray();
		for(int i = 0; i < heros.Length; i++)
		{
			heros[i].shield += percentShield;
		}*/

		shield = true;
	}
}
